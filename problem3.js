module.exports = function (inventory)
{
    if(inventory.length==0){
        return [];
    }
    let carModels = [];

    for (let i = 0; i < inventory.length; i++) {
    carModels.push(inventory[i]["car_model"]);
     }
     return carModels.sort();
};