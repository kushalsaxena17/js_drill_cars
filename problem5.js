module.exports = function (inventory)
{
    if(inventory.length==0){
        return [];
    }
    let oldCars =[];
    for (let i=0; i < inventory.length; i++) {
    if (inventory[i].car_year < 2000) {
        oldCars.push(inventory[i]);
        }
    }
    return oldCars.length;
};